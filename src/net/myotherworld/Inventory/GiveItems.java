package net.myotherworld.Inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GiveItems
{
	
	private HashMap<ItemStack, String> items = new HashMap<ItemStack, String>();
	private HashMap<Integer, ItemStack> joinItems = new HashMap<Integer, ItemStack>();
	private Inventory plugin;
	public GiveItems(Inventory plugin)
	{
		this.plugin = plugin;
		
		loadItems(plugin);
		
	}
	private void loadItems(Inventory plugin)
	{
		ConfigurationSection c = plugin.getConfig().getConfigurationSection("Items");
		if (c == null)
		{
			Bukkit.getLogger().severe("Usun Config.yml i przeladuj ponownie");
			return;
		}
		for (String path_name : c.getKeys(false))
		{
			ConfigurationSection sec = c.getConfigurationSection(path_name);
			
			boolean Join = sec.getBoolean("GiveOnJoin");
			int loc = sec.getInt("Location");
			
			List<String> tempLore = sec.getStringList("Item"); //temp
			List<String> lore = new ArrayList<String>();
			for(String temp : tempLore)
			{
				temp = temp.replace("�", ""); //usuwamy duchy ze znakow specjalnych (dziwne to jest)
				temp = temp.replace("[g]", "�"); //dodajemy duchy okreslone specjalnym znakiem (mindfuck :D)
				lore.add(ChatColor.translateAlternateColorCodes('&', temp));
			}
			int ID = sec.getInt("ID");
			int dur = sec.getInt("Durability");
			int am = sec.getInt("Amount");
			String name = sec.getString("Name").replace("�", ""); //usuwamy duchy ze znakow specjalnych (dziwne to jest)
			name = name.replace("[g]", "�"); //dodajemy duchy okreslone specjalnym znakiem (mindfuck :D)
			
			
			@SuppressWarnings("deprecation")
			ItemStack item = new ItemStack(ID, am, (short) dur);
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
			if (lore != null)
			{
				itemMeta.setLore(lore);
			}
			item.setItemMeta(itemMeta);
			ItemStack i = new ItemStack(item);
			
			//IsInv
			this.items.put(i, null);
			
			if (Join)
			{
				this.joinItems.put(Integer.valueOf(loc), i);
			}
		}
	}
	
	public void reload(Inventory plugin)
	{
		this.items.clear();
		this.joinItems.clear();
		loadItems(plugin);
	}
	
	public void giveJoinItems(Player player)
	{
		//Tego nie tykam bo dziala ale nie wiem jak dziala do konca:)
		if (!this.joinItems.isEmpty())
		{
			//Bukkit.getLogger().severe("Debug2");
			for (Iterator<Integer> localIterator = this.joinItems.keySet().iterator(); localIterator.hasNext();)
			{
				int loc = ((Integer)localIterator.next()).intValue();
				ItemStack is = (ItemStack)this.joinItems.get(Integer.valueOf(loc));
				if ((!player.getInventory().contains(is.getType()))	|| !hasItem(is, player))
				{
					loc--;
					boolean b = false;
					if ((loc >= player.getInventory().getSize()) || (loc < 0))
					{
						loc = 0;
						b = true;
					}
					if ((!b) || (player.getInventory().getItem(loc) == null)) {
						player.getInventory().setItem(loc, is.clone());
						} else {
						player.getInventory().addItem(new ItemStack[] { is.clone() });
					}
				}
			}
		}
	}
	
	//Uzywane do blokowania itemow
	public boolean isInv(ItemStack s)
	{
		if (s == null)
		{
			return false;
		}
		for (ItemStack i : this.items.keySet())
		{
			if (i != null)
			{
				if (i.getType() == s.getType())
				{
					if(s.hasItemMeta())
					{
						if(s.getItemMeta().hasDisplayName())
						{
							if (i.getItemMeta().getDisplayName().equals(s.getItemMeta().getDisplayName()))
							{
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	//Sprawdzenie czy sie ma item w EQ
	private boolean hasItem(ItemStack is, Player player)
	{
		for (ItemStack i : player.getInventory().getContents())
		{
			if(i != null)
			{
				if(i.hasItemMeta())
				{
					if(i.getItemMeta().hasDisplayName())
					{
						if(i.getItemMeta().getDisplayName().equalsIgnoreCase(is.getItemMeta().getDisplayName()))
						{
							if(is.getItemMeta().hasLore() && i.getItemMeta().hasLore())
							{
								if(!is.getItemMeta().getLore().equals(i.getItemMeta().getLore()))
								{
									//Nazwa i reszta sa podobne, wiec zmienmy lore na poprawne - ulatwia to w chuj prace
									ItemMeta tempItemMeta = i.getItemMeta();
									tempItemMeta.setLore(is.getItemMeta().getLore());
									i.setItemMeta(tempItemMeta);
								}
							}
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
}
