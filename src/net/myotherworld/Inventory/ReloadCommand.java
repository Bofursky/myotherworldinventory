package net.myotherworld.Inventory;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ReloadCommand implements CommandExecutor
{
	private Inventory plugin;
	  
	public ReloadCommand(Inventory plugin)
	{
	    this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args)
	{
		if (sender.hasPermission("MyOtherWorldInventory.reload"))
		{
			this.plugin.reload();
			sender.sendMessage(ChatColor.RED + "[INV] Poprawnie przeladowano Config");
		}
		else
		{
			sender.sendMessage(ChatColor.RED + "You do not have permission!");
		}
		return true;
    }
}
