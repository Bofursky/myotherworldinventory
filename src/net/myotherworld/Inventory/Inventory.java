package net.myotherworld.Inventory;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Inventory extends JavaPlugin
{
	private static Inventory instance;
	private PlayerListener listener;
	private GiveItems items;
	
	private void listeners()
	{
		PluginManager pm = getServer().getPluginManager();	
		pm.registerEvents(new PlayerListener(this), this);		
	}
	
	public GiveItems getGiveItems()
	{
		return this.items;
	}
	
	public static Inventory getInstance()
	{
	    return instance;
	}
	
	@Override
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEnable() 
	{  
	    if(getConfig().getString("Settings") == null) 
	    {
	        addDefaultConfig();
	    }
	    instance = this;
	    this.items = new GiveItems(this);
	    this.listener = new PlayerListener(this);
	    
	    listeners();
	    getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	    getCommand("Invreload").setExecutor( new ReloadCommand(this));
	    getCommand("Change").setExecutor( new BungeeCord());
	}
	
	public void reload()
	{
		reloadConfig();
	    if(getConfig().getString("Settings") == null) 
	    {
	        addDefaultConfig();	        
	    }	
        this.items.reload(this);
        this.listener.reload(this);
	}
	  
	private void addDefaultConfig()
	{
		FileConfiguration cs = getConfig();
		
		cs.set("Settings.AllowPlaceItems", Boolean.valueOf(false));
		cs.set("Settings.AllowMoveItems", Boolean.valueOf(false));
		cs.set("Settings.AllowDropItems", Boolean.valueOf(false));
		cs.set("Settings.DropItemsOnDeath", Boolean.valueOf(false));
		cs.set("Settings.GetItemsOnRespawn", Boolean.valueOf(true));
		
		
	    List<String> list = new ArrayList<String>();
	    list.add("Nazwa1");
	    list.add("Nazwa");
	    
	    cs.set("Items.Inventory.Name", "Inventory");  
	    cs.set("Items.Inventory.ID", Integer.valueOf(1));
	    cs.set("Items.Inventory.Durability", Integer.valueOf(1));
	    cs.set("Items.Inventory.Amount", Integer.valueOf(1));
	    cs.set("Items.Inventory.Item", list); 
	    cs.set("Items.Inventory.GiveOnJoin", Boolean.valueOf(true));
	    cs.set("Items.Inventory.Location", Integer.valueOf(9));
	    cs.set("Items.Inventory.Commands", "day");

	    saveConfig();
	}
}
