package net.myotherworld.Inventory;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class BungeeCord implements Listener, CommandExecutor
{
	  public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	  {
		  if(args.length == 0) 
		  {
			  sender.sendMessage("Uzyj /Change [Nazwa serwera]");
		  }
		  else
		  {
			  sendToServer((Player)sender, args[0]);
		  }
		  return true;
	  }
	  
	  public void sendToServer(Player player, String server)
	  {
		  ByteArrayOutputStream b = new ByteArrayOutputStream();
		  DataOutputStream out = new DataOutputStream(b);
		  try
		  {
			  out.writeUTF("Connect");
			  out.writeUTF(server);
		  }
		  catch (Exception e)
		  {
			  e.printStackTrace();
		  }
		  player.sendPluginMessage(Inventory.getInstance(), "BungeeCord", b.toByteArray());
	  }
	  
}

