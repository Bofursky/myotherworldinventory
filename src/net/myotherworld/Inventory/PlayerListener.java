package net.myotherworld.Inventory;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener
{
	 
	private Inventory plugin;
	private boolean drop;
	private boolean move;
	private boolean death;
	private boolean place;
	private boolean respawn;
	
	public PlayerListener(Inventory plugin)
	{
		this.plugin = plugin;
	    loadSettings(plugin);
	}	


	@EventHandler
	public void onJoin(PlayerJoinEvent event)
	{
		 this.plugin.getGiveItems().giveJoinItems(event.getPlayer());
	}	  

	@EventHandler
	public void onDrop(PlayerDropItemEvent event)
	{
		if ((!this.drop) && (this.plugin.getGiveItems().isInv(event.getItemDrop().getItemStack()))) 
		{
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event)
	{
		if (!this.death)
		{
			List<ItemStack> toRemove = new ArrayList<ItemStack>();
			for (ItemStack i : event.getDrops()) 
			{
				if (this.plugin.getGiveItems().isInv(i)) 
				{
					toRemove.add(i);
				}
			}
			for (ItemStack i : toRemove) 
			{
				event.getDrops().remove(i);
			}
			toRemove.clear();			
		}
	}

	@EventHandler
	public void onMove(InventoryClickEvent event)
	{
	    if (!this.move)
	    {	
	    	if (event.getInventory() == null)
	    	{
	    		return;
	    	}
	    	if ((this.plugin.getGiveItems().isInv(event.getCurrentItem())) || (this.plugin.getGiveItems().isInv(event.getCursor()))) 
	    	{    			
	    		event.setCancelled(true);
	    	}
	    }
	    
	    if((this.move && event.isRightClick()) || (!this.move))
	    {
	    	if ((this.plugin.getGiveItems().isInv(event.getCurrentItem()))) 
			{	
				ConfigurationSection c = plugin.getConfig().getConfigurationSection("Items");
				
				for (String path_name : c.getKeys(false))
				{
					ConfigurationSection sec = c.getConfigurationSection(path_name);
				
					//Po kij wykonywac komende, skoro jej np. nie ma ? 
					String cmd = sec.getString("Commands");
					if(cmd.equalsIgnoreCase("") || cmd == null) continue;
					
					String itemName = ChatColor.translateAlternateColorCodes('&', sec.getString("Name"));
					Player player = (Player) event.getWhoClicked();

					if(event.getCurrentItem().getItemMeta().getDisplayName().equals(itemName))
					{
						//Troche uniwersalnie - zamienmy @p na nick gracza oraz obslugujemy konsole
						cmd = cmd.replace("@p", player.getName());
						if(cmd.contains("CONSOLE:"))
						{
							cmd = cmd.replace("CONSOLE:", "");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd);
						}
						else player.performCommand(cmd);
					}
				}
	    	}
	    	else if (this.plugin.getGiveItems().isInv(event.getCursor())) 
			{	
				ConfigurationSection c = plugin.getConfig().getConfigurationSection("Items");
				
				for (String path_name : c.getKeys(false))
				{
					ConfigurationSection sec = c.getConfigurationSection(path_name);
				
					//Po kij wykonywac komende, skoro jej np. nie ma ? 
					String cmd = sec.getString("Commands");
					if(cmd.equalsIgnoreCase("") || cmd == null) continue;
					
					String itemName = ChatColor.translateAlternateColorCodes('&', sec.getString("Name"));
					Player player = (Player) event.getWhoClicked();

					if(event.getCursor().getItemMeta().getDisplayName().equals(itemName))
					{
						//Troche uniwersalnie - zamienmy @p na nick gracza oraz obslugujemy konsole
						cmd = cmd.replace("@p", player.getName());
						if(cmd.contains("CONSOLE:"))
						{
							cmd = cmd.replace("CONSOLE:", "");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd);
						}
						else player.performCommand(cmd);
					}
				}
	    	}
	    }
	    
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event)
	{
	    if ((!this.place) &&  (this.plugin.getGiveItems().isInv(event.getItemInHand()))) 
	    {
	    	event.setCancelled(true);
		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event)
	{
		if (this.respawn)
		{	
			this.plugin.getGiveItems().giveJoinItems(event.getPlayer());
		}
	}
	
	@EventHandler
	public void playerClicked(PlayerInteractEvent event)
	{
		if (((event.getAction() == Action.RIGHT_CLICK_AIR) || 
				(event.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
				(event.getItem() != null))
		{
			if(this.plugin.getGiveItems().isInv(event.getItem()))
			{
				ConfigurationSection c = plugin.getConfig().getConfigurationSection("Items");
				for (String path_name : c.getKeys(false))
				{
					ConfigurationSection sec = c.getConfigurationSection(path_name);
					
					//Po kij wykonywac komende, skoro jej np. nie ma ? 
					String cmd = sec.getString("Commands");
					
					if(cmd.equalsIgnoreCase("") || cmd == null) continue;
					
					String itemName = ChatColor.translateAlternateColorCodes('&', sec.getString("Name"));
					Player player = event.getPlayer();
					
					if(event.getItem().getItemMeta().getDisplayName().equals(itemName))
					{
						//Troche uniwersalnie - zamienmy @p na nick gracza oraz obslugujemy konsole
						cmd = cmd.replace("@p", player.getName());
						if(cmd.contains("CONSOLE:"))
						{
							cmd = cmd.replace("CONSOLE:", "");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd);
						}
						else player.performCommand(cmd);
					}
				}
			}
		}
	}		
	
	public void reload(Inventory plugin)
	{
	    loadSettings(plugin);
	}
	  
	private void loadSettings(Inventory plugin)
	{
		ConfigurationSection s = plugin.getConfig().getConfigurationSection("Settings");
	    
		this.drop = s.getBoolean("AllowDropItems");
	    this.move = s.getBoolean("AllowMoveItems");
	    this.place = s.getBoolean("AllowPlaceItems");
	    this.death = s.getBoolean("DropItemsOnDeath");
	    this.respawn = s.getBoolean("GetItemsOnRespawn");
	   	  
	}
}
